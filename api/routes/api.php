<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'AuthController@register');
Route::post('logout', 'AuthController@logout')->middleware('jwt.auth');

Route::group(['middleware' => 'api'], function () {
    Route::post('login', 'AuthController@login');
});

Route::group(['prefix' => 'posts'], function () {
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('/', 'PostController@store');
        Route::patch('/{post}', 'PostController@update');
        Route::delete('/{post}', 'PostController@destroy');

        Route::post('/{post}/comments', 'CommentController@store');
        Route::patch('/{post}/comments/{comment}', 'CommentController@update');
        Route::delete('/{post}/comments/{comment}', 'CommentController@destroy');
    });
});
