<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'creator_id', 'body', 'parent_id'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }
}
