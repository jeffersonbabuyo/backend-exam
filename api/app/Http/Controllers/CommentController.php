<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $comment = new Comment;

        $data = array_merge(
            [
                'creator_id' => $request->user()->id
            ],
            $request->only('body', 'parent_id')
        );

        foreach ($data as $key => $value) {
            // if ($key === 'parent_id') {
            //     try {
            //         $parent = Comment:where('id', $value)->firstOrFail();
            //         $parent['children'] => [
            //             ''
            //         ];
            //     } catch () {

            //     }
            //     break;
            // } else {
            //     $comment[$key] = $value;
            // }

            $comment[$key] = $value;
        }

        $post->comments()->save($comment);

        return new CommentResource($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post, Comment $comment)
    {
        $comment->update($request->only('body'));

        return new CommentResource($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post, Comment $comment)
    {
        $comment->delete();

        return response()->json(['status' => 'record deleted successfully']);
    }
}
